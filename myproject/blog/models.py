from django.utils import timezone
from django.db import models

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name = "Subject")
    slug = models.SlugField(max_length=100, unique=True, verbose_name ="category adress")
    status = models.BooleanField(default=True,verbose_name="status" )
    position = models.IntegerField(verbose_name= "amount")
    class Meta:
        verbose_name = "cat"
        verbose_name_plural = "categories"
        ordering =['position']
    def __str__(self): # return the name we will object calls
        return self.title

class Article(models.Model):
    STATUS_CHOICES = (
        ('d','Draft'),
        ('p',"Published"),
    )
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=100, unique= True)
    category = models.ManyToManyField(Category, verbose_name='category', related_name="articles")
    # manay to many relation og makemigration bagefter
    description = models.TextField()
    thumbnail = models.ImageField(upload_to="images")
    #video = models.FileField(upload_to="images")
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)

    def __str__(self): # return the name we will object calls
        return self.title

    def category_to_str(self): # def category_to_str(self, obj): obj in admin betyder hver object men under models bruges ikke
        return ", ".join([Category.title for Category in self.category.all()])
    class Meta:
        verbose_name = "artikel_cat" #
        verbose_name_plural = "article_categories" # navn som vil stå under blog under adminside

'''
    def jpublish(self):
        return jalali_Converter(self.publish)
    jpublish.short_description ="time"
'''


