from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse #, JsonResponse
from .models import Article, Category

def index (request):
    return render(request, "blog/index.html")

def home (request):
    context = {
        "articles": Article.objects.all(), # kallder alle ellementer
        #"articles": Article.objects.filter(status="p").order_by('publish')[:1]# configure data.. det kan ordered i admin også
        "category": Category.objects.filter(status=True)
    }
    return render(request, "blog/home.html", context)

def detail(request, slug):
    context = {
        "article": Article.objects.get(slug=slug)  # den tage en object alle ellementer
    }
    return render(request, "blog/detail.html", context)
def category(request, slug):
    context = {
        "category": Category.objects.filter(status=True)# den tage en object alle ellementer
    }
    return render(request, "blog/category.html", context)



# Create your views here.
"""
def home (request):
    context = {
        "articles": Article.objects.all() # kallder alle ellementer
        #"articles": Article.objects.filter(status="p").order_by('publish')[:1]# configure data
    }
    return render(request, "blog/home.html", context)
def detail(request, slug):
    context = {
        "article": Article.objects.get(slug=slug)  # den tage en object alle ellementer
    }
    return render(request, "blog/detail.html", context)
def home (request):
    
    context = {
        "articles": [
            {
                "name":"jjjjj",
                "age":22,
                 "img": "https://static.farakav.com/files/pictures/thumb/01555538.jpg"

            },
            {
                "name": "jioiojj",
                "age": 28,
                "img": "https://static.farakav.com/files/pictures/thumb/01523029.jpg"

            }
        ]
    }

    return render(request, "blog/home.html", context)
"""
