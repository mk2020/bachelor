# Register your models here.
from django.contrib import admin
from .models import Article,Category

class CategoryAdmin(admin.ModelAdmin):

    list_display = ('position','title','slug','status') # columnsname for at vises i table
    list_filter = (['status']) # filter
    search_fields = ('title','slug') # søge resultalter from a column
    prepopulated_fields = {'slug':('title',)} # all title saves in slug
    ordering = ['position'] # sort some of columns!
admin.site.register(Category , CategoryAdmin)# registere class her og derefter migrate

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title','slug','publish','status','category_to_str') #columnsname for at vises i table
    list_filter = ('publish','status') # filter
    search_fields = ('title','description') # søge resultalter from a column
    prepopulated_fields = {'slug':('title',)} # all title saves in slug
    ordering = ['-status','-publish'] # sort some of columns!
    '''
    def category_to_str(self, obj):
        return ", ".join([Category.title for Category in obj.category.all()])
    '''
admin.site.register(Article , ArticleAdmin)

