
from django.urls import path, re_path
from .views import home, detail, index,category

app_name ="blog"
urlpatterns = [
    path('',home, name="home"),
    path('index',index, name="index"),
    path('detail/<slug:slug>',detail, name ="detail"),
    path('category/<slug:slug>', category, name ="category")
]