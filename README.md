# Django

### Required packages
```bash
. pycharm

. python 
```

### To install all the scripts:
```sh
 pip install -r requirements.txt
```

### To run Django projekt
```python

 cd <this directory>/.venv^

 activate.bat

 cd ../bachelor
 
 python manage.py runserver
 ```


# Path:

### login

```sh
127.0.0.1:8000/account
```
```sh
127.0.0.1:8000/account/login
```
### Blog

```sh
127.0.0.1:8000
```
```sh
127.0.0.1:8000/index
```
```sh
127.0.0.1:8000/detail/<slug:slug>
```
```sh
127.0.0.1:8000/category/<slug:slug>
```
 


